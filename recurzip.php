<?php

define('ROOT', __DIR__);
define('DS', DIRECTORY_SEPARATOR);
define('TMP_DIR', __DIR__ . DS . 'tmp');
define('OUTPUT_DIR', __DIR__ . DS . 'zip');

class Zipper {

    protected $directory;
    protected $outputDIR;
    protected $ignore;

    public function __construct($directory, $outputDIR)
    {
        $this->directory = $directory;
        $this->outputDIR = $outputDIR;

        $this->ignore = [
            dirname(ROOT),
            ROOT,
            TMP_DIR,
            OUTPUT_DIR,
            ROOT . DS . 'nbproject',
            ROOT . DS . '.git'
        ];
    }

    protected function iterator()
    {
        return new \DirectoryIterator($this->directory);
    }

    public function zip()
    {
        $iterator = $this->iterator();

        foreach ($iterator as $file) {

            if ($file->isDot()) {
                continue;
            }
            if (in_array($file->getRealPath(), $this->ignore)) {
                continue;
            }

            if ($file->isDir()) {
                $this->zipDirectory($file->getRealPath());
                continue;
            }
        }
    }

    public function zipDirectory($directory)
    {
        $zipper = new \ZipArchive();

        $zipper->open($this->outputDIR . DS . basename($directory) . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($directory), \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $file) {

            if ($file->isFile()) {

                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($this->directory) + 1);

                $zipper->addFile($filePath, $relativePath);
            }
        }

        $zipper->close();
    }

}

class RecurZip {

    protected $zipper;

    public function __construct()
    {
        $this->zipper = new Zipper(ROOT, TMP_DIR);
    }

    public function zip()
    {
        $this->bootstrap();
        /**
         * Lets zip source file to temp dir
         */
        $this->zipper->zip();

        /**
         * Now Zip tmp dir to final output
         */
        $zipper = new Zipper(TMP_DIR, OUTPUT_DIR);
        $zipper->zipDirectory(TMP_DIR);

        /**
         * Clean Temporary Files
         */
        $this->cleanTmp();
    }

    public function bootstrap()
    {
        if (is_dir(TMP_DIR) === false) {
            mkdir(TMP_DIR, 0755);
        }

        if (is_dir(OUTPUT_DIR) === false) {
            mkdir(OUTPUT_DIR, 0755);
        }
    }

    public function cleanTmp()
    {
        $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator(TMP_DIR), \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $file) {
            if ($file->isFile()) {
                unlink($file->getRealPath());
            }
        }
    }

}

$recurzip = new RecurZip();
$recurzip->zip();
